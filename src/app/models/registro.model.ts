export class Registro{
    format:string;
    text:string;
    type:string;
    icon:string;
    created:Date;
    constructor(format:string,text:string){
        this.created = new Date();
    }
    private determinarTipo(){
        const inicioTexto = this.text.substr(0,4);
        switch(inicioTexto){
            case 'http':
                this.type = 'http';
                this.icon = 'globe';
                break;
            case 'http':
                this.type = 'geo';
                this.icon = 'pin';
                break;
            default:
                this.type = 'No reconocido';
                this.icon = 'create';
        }
    }
}