import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(private barcodeScanner:BarcodeScanner,public dataLocal: DataService) {}
  scan(){
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data',barcodeData);
      if(!barcodeData.cancelled){
        //this.dataLocal.guardarRegistros(barcodeData.format,barcodeData.text);
      }
    }).catch(err => {
      console.log('Error',err);
      //this.dataLocal.guardarRegistros('QRCode','https://ionicframework.com/');
      //this.dataLocal.guardarRegistros('QRCode','geo');
    });
   
  }
  

}
